/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9174605728999035, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.1618217054263566, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9992772667542706, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.7810781078107811, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7626728110599078, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.07380073800738007, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7629533678756477, 500, 1500, "me"], "isController": false}, {"data": [0.7903765690376569, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.7050691244239631, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.7583955223880597, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 31070, 0, 0.0, 488.27653685226795, 10, 11767, 62.0, 420.0, 3018.0, 8724.75000000004, 101.0932517732804, 254.76113460459104, 101.83078936906196], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 516, 0, 0.0, 2921.837209302326, 515, 9941, 2158.0, 6335.900000000001, 7107.649999999997, 8003.769999999998, 1.6858282611465594, 1.065166879845531, 2.0858832098366116], "isController": false}, {"data": ["getLatestMobileVersion", 22830, 0, 0.0, 65.2860271572494, 10, 1461, 47.0, 125.0, 162.0, 253.0, 76.1238521403373, 50.92269405872173, 56.1264730136276], "isController": false}, {"data": ["findAllConfigByCategory", 1818, 0, 0.0, 833.8047304730475, 36, 8147, 211.0, 2806.600000000002, 4253.3499999999985, 6737.0, 5.957218129866044, 6.736775971078984, 7.7374024538299215], "isController": false}, {"data": ["getNotifications", 868, 0, 0.0, 1752.6394009216588, 62, 9685, 288.0, 8331.5, 8647.65, 9042.72, 2.825346088620821, 23.488448488700243, 3.1426456981827298], "isController": false}, {"data": ["getHomefeed", 271, 0, 0.0, 5631.405904059044, 990, 11767, 6762.0, 9026.8, 9307.6, 11118.279999999999, 0.882015030056859, 10.746974150409601, 4.413520521495455], "isController": false}, {"data": ["me", 1158, 0, 0.0, 1307.0768566493934, 65, 8904, 267.0, 5535.7000000000035, 7707.649999999998, 8467.660000000002, 3.7911899032559053, 4.9945539243971915, 12.01407347272013], "isController": false}, {"data": ["findAllChildrenByParent", 1195, 0, 0.0, 1273.7690376569042, 45, 8885, 238.0, 5835.800000000007, 8151.000000000001, 8615.2, 3.8901765716964425, 4.368850642041902, 6.215164913374394], "isController": false}, {"data": ["getAllClassInfo", 651, 0, 0.0, 2343.0199692780316, 85, 10204, 368.0, 8725.0, 9031.0, 9483.000000000005, 2.1242784468946705, 6.055674078051734, 5.453835973521571], "isController": false}, {"data": ["findAllSchoolConfig", 1608, 0, 0.0, 942.6386815920405, 41, 8339, 243.0, 3277.9000000000015, 4611.449999999999, 6920.55, 5.2691251548297044, 114.91220991958687, 3.8643681555440508], "isController": false}, {"data": ["getChildCheckInCheckOut", 155, 0, 0.0, 9887.270967741937, 8325, 11358, 9880.0, 10600.4, 10877.999999999998, 11338.96, 0.5043422238563633, 33.626919271282425, 2.320762264464047], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 31070, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
